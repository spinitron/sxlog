@echo off

@setlocal

REM You might want to set the PHP_COMMAND variable here like this:
REM PHP_COMMAND="c:\PHP\php.exe"

if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe

"%PHP_COMMAND%" -f "sxlog.php" -- %*

@endlocal
