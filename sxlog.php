<?php
/**
 * SXlog Stream log parser, ATH calculator and NPR streaming log converter
 *
 * @link https://bitbucket.org/spinitron/sxlog
 * @author Tom Worster tom@spinitron.com
 * @copyright Copyright (c) 2015, Spinitron LLC
 * @license http://opensource.org/licenses/ISC
 *
 * Copyright (c) 2015, Spinitron LLC
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby
 * granted, provided that the above copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
$timeZone = @ini_get('date.timezone');

$options = array(
    'mountpoint' => 'listen',
    'stream-id' => null,
    'log-dir' => '~/Library/Logs/Nicecast',
    'begin' => null,
    'end' => null,
    'output' => null,
    'time-zone' => $timeZone ? $timeZone : 'America/New_York',
    'min-connect' => 3,
    'max-connect' => '1 year',
    'anon' => false,
);

$scriptName = basename(array_shift($argv));
$usage = <<<TXT
usage: $scriptName [--log-dir=<path>] [--output=<path>] [--anon]
   [--begin=<datetime>] [--end=<datetime>] [--time-zone=<timezone>]
   [--mountpoint=<name>] [--stream-id=<name>]
   [--min-connect=<number>] [--min-connect=<time-interval>]
Make NPR streaming logs from server logs and compute ATH.

Options [default value]:
  --log-dir      Search <path> for IceCast log files. [{$options['log-dir']}]
  --output       Output SX log file path. [Standard output]
  --anon         Anonymize output by removing client IP addresses
  --begin        Beginning of the report period. [Earliest log]
  --end          End of the report period. [latest log]
  --time-zone    Time zone you use for --begin and --end options [{$options['time-zone']}]
  --mountpoint   Ignore connections to IceCast mountpoints except <name> [{$options['mountpoint']}]
  --stream-id    Use <name> as the service name in the output log [use mountpoint name]
  --min-connect  Ignore connections shorter than <number> seconds [{$options['min-connect']}]
  --max-connect  Assume connections are not longer than <time-interval> [{$options['max-connect']}]

TXT;

if (in_array('--help', $argv)) {
    echo $usage;
    exit;
}

/**
 * Write line to stderr.
 *
 * @param string $message
 * @param int|null $code Exit status code or null to not exit.
 */
function stderr($message, $code = null)
{
    global $scriptName;
    fwrite(STDERR, $message . "\n");
    if ($code !== null) {
        fwrite(STDERR, 'For a help summary, use: ' . $scriptName . " --help\n");
        exit($code);
    }
}

/**
 * Wrap strtotime() in error checking.
 *
 * @param string $string Date/time string to parse.
 *
 * @return int Unit time
 */
function toUnix($string)
{
    $time = strtotime($string);
    if (!$time || $time == -1) {
        stderr('Invalid date-time: ' . $string, 65);
    }

    return $time;
}

/**
 * @param string $fileName
 *
 * @return string
 */
function fileReadLastLine($fileName)
{
    $f = fopen($fileName, 'r');
    $cursor = -1;
    fseek($f, $cursor, SEEK_END);
    $char = fgetc($f);

    // Trim trailing newline chars of the file
    while ($char === "\n" || $char === "\r") {
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
    }

    $line = '';

    // Read until the start of file or first newline char
    while ($char !== false && $char !== "\n" && $char !== "\r") {
        // Prepend the new char
        $line = $char . $line;
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
    }

    fclose($f);

    return $line;
}

/**
 * Process command line arguments.
 */
foreach ($argv as $arg) {
    if (preg_match('{^--([^=]+)(?:=(.+))?$}', $arg, $matches)) {
        if (array_key_exists($matches[1], $options)) {
            $options[$matches[1]] = isset($matches[2]) ? $matches[2] : true;
        } else {
            stderr('Unknown option: ' . $matches[1], 64);
        }
    }
}

// Check the timezone. This throws an exception if tz is invalid.
$ignore = new DateTimeZone($options['time-zone']);
date_default_timezone_set($options['time-zone']);

// Check and convert report period inputs
if ($options['begin'] !== null) {
    $options['begin'] = toUnix($options['begin']);
}
if ($options['end'] !== null) {
    $options['end'] = toUnix($options['end']);
}

// convert max-connect to seconds.
$options['max-connect'] = strtotime('today') - toUnix('today - ' . $options['max-connect']);

// Open the output file, if given.
if ($options['output']) {
    $out = fopen($options['output'], 'w+');
    if ($out === false) {
        stderr('Cannot open output file: ' . $options['output'], 74);
    }
} else {
    $out = STDOUT;
}

// Open the directory containing IceCast log files.
if (!is_dir($options['log-dir'])) {
    stderr('Cannot find directory: ' . $options['log-dir'], 74);
}
$dh = @opendir($options['log-dir']);
if ($dh === false) {
    stderr('Cannot open directory: ' . $options['log-dir'], 74);
}

// Warn if GeoIP or its database is absent.
$geoip = function_exists('geoip_country_name_by_name');
if (!$geoip) {
    stderr('PHP Geo IP Location extension is not available.');
} else {
    $geoip = geoip_country_code_by_name('74.50.101.15') === 'US';
    if (!$geoip) {
        stderr('Geo IP Location database is not available.');
    }
}
if (!$geoip) {
    stderr('Stream clients outside the US are included in output.');
}

/*
 * Regex to parse IceCast log file lines.
 *  12.123.23.213 - - [05/Jan/2015:15:52:59 -0500] "GET /listen HTTP/1.1" 200 66994502 "http://xyz.org/" "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko" 4187
 *  1  hostname       12.123.23.213
 *  2  date-time      05/Jan/2015:15:52:59 -0500
 *  3  request        GET /listen HTTP/1.1
 *  4  status code    200
 *  5  response size  66994502
 *  6  Referrer       http://wmrw.org/
 *  7  User agent     Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko
 *  8  Duration       4187
 */
$logPattern = '{^([^ ]+) - - \[([^\]]+)\] "([^"]+)" (\d+) (\d+) "([^"]*)" "([^"]*)" (\d+)$}';

/*
 * Shoutcast w3c log files
 * #Fields: c-ip c-dns date time cs-uri-stem c-status cs(User-Agent) sc-bytes x-duration avgbandwidth
 * 127.0.0.1 127.0.0.1 2015-05-07 16:51:19 /stream?title=Unknown 200 VLC%2F2.2.1%20LibVLC%2F2.2.1 0 0 0
 * 127.0.0.1 127.0.0.1 2015-05-07 16:51:19 /stream?title=Unknown 200 NSPlayer%2F7.10.0.3059 0 0 0
 */

/** @var int Count down the files as progress indicator. */
$numFiles = count(scandir($options['log-dir']));

/** @var int Unix time of first available log entry */
$latest = 0;

/** @var int Unix time of last available log entry */
$earliest = strtotime('tomorrow');

/** @var int Accumulate the client connection durations */
$listenerSeconds = 0;

stderr("Working. Files to go... ");
do {
    fwrite(STDERR, $numFiles . ', ');
    $numFiles -= 1;

    $file = readdir($dh);

    if ($file !== false && is_file($options['log-dir'] . '/' . $file)) {
        $fh = @fopen($options['log-dir'] . '/' . $file, 'r');
        if ($fh === false) {
            stderr('Cannot open file: ' . $file, 74);
        }

        /** @var int Number of lines that didn't match as expected */
        $badLines = 0;

        /** @var int Number of lines that did */
        $goodLines = 0;

        do {
            $inLine = fgets($fh);

            if (preg_match($logPattern, $inLine, $matches)) {
                // Figure the begin and end of the client connection.
                $end = toUnix($matches[2]);
                $begin = $end - $matches[8];

                // Capture the earliest and latest log entry timestamps. $end in both is correct.
                if ($end > $latest) {
                    $latest = $end;
                }
                if ($end < $earliest) {
                    $earliest = $end;
                }

                // If the last connection in the file ended before the start of the reporting
                // period, skip the file. This optimization only works if the
                if ($goodLines === 0 && $options['begin']) {
                    $lastLine = fileReadLastLine($options['log-dir'] . '/' . $file);
                    if (preg_match($logPattern, $lastLine, $matches)) {
                        $fileEnds = strtotime($matches[2]);
                        if ($fileEnds && $fileEnds < $options['begin']) {
                            // Capture the earliest and latest log entry timestamps. $end in both is correct.
                            if ($fileEnds > $latest) {
                                $latest = $fileEnds;
                            }
                            if ($fileEnds < $earliest) {
                                $earliest = $fileEnds;
                            }

                            // Cause file to be skipped.
                            $inLine = false;
                        }
                    }
                }

                // If the current connection ended more than the max-connection time after
                // the end of the reporting period, skip to the end of the file.
                if (($options['end'] && $end - $options['max-connect'] > $options['end'])) {
                    $inLine = false;
                }

                $goodLines += 1;

                // Conditions for including the connection in the output log. It must be fore the
                // given mountpoint and be not too short and be from the USA (if we can tell) and
                // be at least partially inside the reporting period.
                if ($inLine
                    && $matches[8] >= $options['min-connect']
                    && strpos($matches[3], 'GET /' . $options['mountpoint']) === 0
                    && (!$options['end'] || $begin < $options['end'])
                    && (!$options['begin'] || $end > $options['begin'])
                    && $matches[4] === '200'
                    && (!$geoip || @geoip_country_code_by_name($matches[1]) === 'US')
                ) {
                    // If the connection end is after the reporting period end, trim it.
                    if ($options['end'] && $end > $options['end']) {
                        $end = $options['end'];
                    }

                    // If the connection begin is before the reporting period begin, trim it.
                    if ($options['begin'] && $begin < $options['begin']) {
                        $begin = $options['begin'];
                    }

                    // "Tuning seconds" for this connection
                    $duration = $end - $begin;

                    $listenerSeconds += $duration;

                    // Build the output log line according to
                    // http://digitalservices.npr.org/streaming-log-file-guidelines
                    $outLine = array();
                    $outLine[] = $options['anon'] ? '-' : $matches[1];
                    $outLine[] = gmdate('Y-m-d', $begin);
                    $outLine[] = gmdate('H:i:s', $begin);
                    $outLine[] = $options['stream-id'] === null ? $options['mountpoint'] : $options['stream-id'];
                    $outLine[] = $duration;
                    $outLine[] = $matches[4];
                    // Column 7 "referrer" seems not required.
                    $outLine[] = '';
                    fwrite($out, implode("\t", $outLine) . "\n");
                }
            } else {
                // The input line doesn't match the expected IceCast log pattern. Suspicious.
                $badLines += 1;
                if ($badLines > 5) {
                    // Too many non-matching lines. Either this isn't a log file or there's a problem.

                    // If any lines matched the pattern and more than 5 didn't then either we have a
                    // corrupt log file or, more likely, a bug in this script.
                    if ($goodLines > 0) {
                        stderr('To many lines with unrecognized pattern, aborting file: ' . $file);
                        stderr('This might be a bug. Might be wise to report it.');
                    }

                    // This will terminate the inner while loop.
                    $inLine = false;
                }
            }
        } while ($inLine !== false);

        fclose($fh);
    }
} while ($file !== false);

fwrite(STDERR, "\n");

stderr('Dates and times in output are in time zone: ' . $options['time-zone']);
stderr('Ignoring client connections shorter than:   ' . $options['min-connect'] . ' seconds');
stderr('Earliest available log: ' . date('Y-m-d H:i:s', $earliest));
stderr('Latest available log:   ' . date('Y-m-d H:i:s', $latest));
if ($options['begin']) {
    stderr('Report period begins:   ' . date('Y-m-d H:i:s', $options['begin']));
}
if ($options['end']) {
    stderr('Report period ends:     ' . date('Y-m-d H:i:s', $options['end']));
}
stderr(
    'Total listener seconds: ' . $listenerSeconds . ' = ' . number_format($listenerSeconds / 3600, 3) . ' hours (ATH)'
);
