# SXlog

SXlog processes IceCast server log files to

- calculate Aggregate Tuning Hours (ATH)
- filter log entries
- write a "streaming log file" according to 
[NPR requirements](http://digitalservices.npr.org/streaming-log-file-guidelines)

We hope to add support for other log file formats such as Shoutcast W3C logs one day.


## Installation

SXlog requires PHP. To filter out client connections from outside the USA it also 
requires the PHP GeoIP extension and a GeoIP database.

### Mac OS X

OS X includes PHP which will run sxlog.php. To add GeoIP I think it is easiest to use
[Homebrew](http://brew.sh/) to install (another instance of) PHP and GeoIP. Homebrew can also 
install a GeoIP database.

For a Homebrew install of PHP:

- Install Homebrew using [these instructions](http://brew.sh/)
- Check Homebrew with `brew doctor`, correct any issues and recheck until the doctor is happy.
- Install PHP 5.6 using [these instructions](https://github.com/Homebrew/homebrew-php#installation).
    Unless you have some other need for them, you can use  
            `--without-apache --without-fpm --without-ldap --without-mysql`
- Install GeoIP `brew install php56-geoip`
- Install GeoIP Update `brew install geoipupdate`
- If you haven't already, change your shell PATH to put `/usr/local/bin` before `/usr/bin`

To use the download ZIP file (rather than git):

- In the Bitbucket page for SXlog, click the "cloud-arrow" icon in the left panel.
- Click the "Download repository" link, which should give you a file called
something like "spinitron-sxlog-4e0389e65dce.zip" in your Downloads folder.
- Unzip the downloaded file if it didn't unzip automatically. You should now
have a folder with the same name as the ZIP file.
- Open a Terminal window (Terminal is an app in the Utilities folder in Applications)
or you can find it with Spotlight (⌘-space then type "terminal").
- Enter these commands:

        cd ~/Downloads
        cd spinitron-sxlog-  # and press the TAB key, it should expand with some random chars
        ./sxlog --help
        
    if you see a help message then things would appear to be working.
    
- Proceed with Using SXlog below

### Windows

I have no experience with it but the PHP web site has detailed instructions to
[install PHP](http://php.net/manual/en/install.windows.manual.php). This won't have GeoIP.

For the time being, I can't help. There's lots of info on installing PHP on windows on
the web but be careful with it. A lot of it is garbage and most of is about setting
up PHP together with Apache and MySQL, which you probably don't need.


### Linux, FreeBSD, etc.

You already know how to install and use PHP or how to find out. If not, why Linux?

## Using SXlog

SXlog reads server log files from the directory you specify with the `--log-dir` option. All
log files relevant to the report period must be in this directory. 
It writes the output log either to standard output or to the file specified by `--output`.
It writes status and informational messages, including ATH summary, to standard error.

Use `sxlog --help` to print a summary help like the following.

    usage: sxlog [--log-dir=<path>] [--output=<path>] [--anon]
       [--begin=<datetime>] [--end=<datetime>] [--time-zone=<timezone>]
       [--mountpoint=<name>] [--stream-id=<name>]
       [--min-connect=<number>] [--min-connect=<time-interval>]
    Make NPR streaming logs from server logs and compute ATH.

    Options [default value]:
      --log-dir      Search <path> for stream server log files. [~/Library/Logs/Nicecast]
      --output       Output SX log file path. [Standard output]
      --anon         Anonymize output by removing client IP addresses
      --begin        Beginning of the report period. [Earliest log]
      --end          End of the report period. [latest log]
      --time-zone    Time zone you use for --begin and --end options [America/New_York]
      --mountpoint   Ignore connections to mountpoints except <name> [listen]
      --stream-id    Use <name> as the service name in the output log [use mountpoint name]
      --min-connect  Ignore connections shorter than <number> seconds [3]
      --max-connect  Assume connections are not longer than <time-interval> [1 year]
      
### Specify report period with `--begin` and `--end`

If you do not specify `--begin` then the report period starts with the earliest 
available log. Similarly, if you don't specify `--end` then the report period runs to 
the end of the available logs.

`--begin` and `--end` can be 
[almost anything reasonable](http://www.gnu.org/software/tar/manual/html_node/Date-input-formats.html).
You can use date or date-times but if you use a date without specifying a time, it is understood 
as midnight at the start of that day, e.g. 

    --end="Jan 31st 2015"
     
means 00:00 on Jan 31st 2015 in whatever time zone `--time-zone` is set to. So
logs for Jan 31st itself are not in the reporting period. 
You can use relative dates, e.g. 

    --begin="Jan 1 2015" --end="Jan 1 2015 + 1 month"

sets the 
report period to all of January 2015 and

    --end="Apr 1 2015" --begin="Apr 1 2015 - 2 weeks"

chooses the last two weeks of March 2015.

Remember to "quote" values with spaces,
e.g. `--end=2015-02-01` is ok but `--end=Jan 31st 2015` isn't. 

Unless you include a time zone in the value, e.g.

    --end="Apr 1 2015 UTC+5:00"

then the value of `--time-zone` is used. You can
find its default value from `sxlog --help`.

### Other options

The `--mountpoint` name selects from the log files one 
[IceCast mountpoint](http://icecast.org/docs/icecast-2.4.1/basic-setup.html)
filtering out all others. You have to get this right or you won't get any results
or you'll get completely wrong results.

NPR logs require a column called Stream ID. You can specify this with `--stream-id` or
SXlog writes the mountpoint name here.

SXlog uses `--min-connect` to filter out extremely short connections. Set this to 0 to
include everything.

The `--max-connect` option tells SXlog to assume there are no connections longer than 
this value. SXlog uses it to save time
by not processing log files in the log file directory
that are irrelevant to the report period. Server log entries
are written at the moment a client *disconnects* from the server and the log entry 
notes the connection's duration. A client connection can end after the report
period and begin within or before it. So SXlog needs to process log entries timestamped
after the report period. The `--max-connect` option controls how long after the end of the
report period SXlog goes before skipping the rest of the logs. If you are processing
a lot of data and it is taking too long and you know that connection times are limited
then you can set this option. Its default value (which `sxlog --help` displays) is large
to be on the safe side.

## Notes

### Filtering out clients outside the USA

SoundExchange only deals with payment of royalties based on listeners in the USA.
So clients outside the USA should not count towards ATH or ATP. If you are unable to
get the PHP GeoIP feature working (SXlog displays a warning if not) you should be
aware of the consequences:

- As far as reporting to NPR is concerned, they do GeoIP filtering if you include
client IP addresses in the log, so the only issue there is privacy, mentioned below.
- The calculated ATH value may be higher than it should be.

If you are reporting directly to SoundExchange, not through NPR, you need two different 
ATH values:

1. *Monthly ATH* to demonstrate eligibility for the rates and 
terms you are using.
2. *ATH for the report period* to include in your quarterly reports.

If the *monthly ATH* including non-USA clients is under the monthly threshold for your rates and terms
then you are
compliant. Only when you are over the monthly threshold for your rates and terms
is it important to get GeoIP working so you can filter out the non-USA clients
and maybe discover that you are in fact compliant.

If the *ATH for the report period* is higher than it should be owing to clients outside
the USA listening to your stream, it skews SoundExchange's calculation of per-artist
royalty payments a little bit. SoundExchange has more than once in the past said that 
they prefer to get reports with some inaccuracies to none at all. So it seems 
reasonable to ask them if it's ok for you to file reports with ATH including
non-USA clients.

### NPR-format logs

NPR's [description of their streaming log file format](http://digitalservices.npr.org/streaming-log-file-guidelines)
does not say how to log connections that start before and/or end after the report period.
SXlog adjusts start and end of connections that are partially outside the report period.

The seventh column of the log is described as:

> Referrer/Client Player (Free text, anything user agent or referrer will work)

which implies that it is optional. Referrer and user agent are irrelevant to SoundExchange.
So SXlog leaves this field blank.

### Client privacy

You may encounter privacy issues with client IP addresses, so SXlog provides the
`--anon` option to remove these from the output log file.

NPR reqiires client IP addresses in streaming logs but their 
[FAQ](http://digitalservices.npr.org/soundexchange-faqs#ip_addresses)
says they require this in order to filter out clients based outside the USA. SXlog does
this filtering anyway, if you have the PHP GeoIP extension and a GeoIP database, or it 
displays a warning if you do not. If you don't see that warning then NPR might (we don't 
know) accept
logs generated with the `--anon` option if you tell them you have already filtered 
out non-USA clients.

### Time zones

NPR requires UTC date and time values and SXlog provides them. But 
report periods are usually for some number of days and we assume these days do not 
begin and end at midnight UTC. We assume you should use your station's local time for the 
`--begin` and `--end`. If the default (which `sxlog --help` displays) is not correct,
specify it using [one of these](http://php.net/manual/en/timezones.php).

## License

Copyright (c) 2015, Spinitron LLC

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby
granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

